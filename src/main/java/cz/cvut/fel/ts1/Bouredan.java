package cz.cvut.fel.ts1;

public class Bouredan {

    public static long factorial(int n) {
        return factorialRec(n, 1);
    }

    private static long factorialRec(int n, int result) {
        if (n == 1) {
            return result;
        }
        return factorialRec(n - 1, n * result);
    }
}
